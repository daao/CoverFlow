using UnityEngine;
using System.Collections;

public interface IClickEventListener
{
	void OnClickEvent( );
	void OnHoverEvent( bool a_bIsHovered );
}
